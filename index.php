<?php

$input_file = 'csv/input.csv';
$output_file = 'csv/output.csv';
const NUM_LINE = 5;
const PARSE_HEADER = true;
const DELIMITER = ',';

$read = new readCsv($input_file, PARSE_HEADER, DELIMITER, $output_file);
$read->getUniquePhoneCsv();


class readCsv 
{ 
    private $file; 
    private $parse_header; 
    private $header; 
    private $delimiter; 
	private $length;
	private $output_file;
	private $full_line = []; 
    //-------------------------------------------------------------------- 
    function __construct($file_name, $parse_header=false, $delimiter="\t", $output_file, $length=8000) 
    { 
        $this->file = fopen($file_name, "r"); 
        $this->parse_header = $parse_header; 
        $this->delimiter = $delimiter; 
		$this->length = $length;
		$this->output_file = $output_file;

        if ($this->parse_header) 
        { 
           $this->header = fgetcsv($this->file, $this->length, $this->delimiter); 
        } 

    } 
    //-------------------------------------------------------------------- 
    function __destruct() 
    { 
        if ($this->file) 
        { 
            fclose($this->file); 
        } 
	}
	
	
	function getUniquePhoneCsv () {
		$success = $this->file_get_contents_chunked($this->file, $this->length, function($chunk, &$handle, $iteration, $output_file) {
			$data = [];
			if (!empty($chunk)) {
				foreach ($chunk as $item){
					if (!isset($this->full_line[$item[0]])){
						$data[$item[0]] = [
							$item[0], $item[1]
						];
						$this->full_line[$item[0]] = [
							$item[0], $item[1], $item[3]
						];
					} 
					else if (isset($this->full_line[$item[0]]) && !in_array($item[3], $this->full_line[$item[0]])) {
						$data[$item[0]] = [
							$item[0], $item[1]
						];
						$this->full_line[$item[0]] = [
							$item[0], $item[1], $item[3]
						];
					} else if (isset($this->full_line[$item[0]]) && in_array($item[3], $this->full_line[$item[0]]) && strtotime($this->full_line[$item[0]][1]) > strtotime($item[1])) {
					
						$data[$item[0]] = [
							$item[0], $item[1]
						];
						$this->full_line[$item[0]] = [
							$item[0], $item[1], $item[3]
						];
					}
				}
			}
			$this->createCsv($data, $output_file);			
		});
		if(!$success)
		{
			echo 'fail';
		}
		unset($this->full_line);
	}

	private function file_get_contents_chunked($handle,$chunk_size, $callback)
	{
		try
		{
			if(file_exists($this->output_file)) {
				unlink($this->output_file);
			}
			$output_file = fopen($this->output_file,"a+");
			fputcsv($output_file, ['PHONE_NUMBER', 'REAL_ACTIVATION_DATE']);

			$i = 0;
			while (!feof($handle))
			{
				call_user_func_array(
					$callback,
					array(
						$this->getLine($handle,$chunk_size, NUM_LINE),
						&$handle,
						$i,
						&$output_file
					)
			);
				$i++;
			}
			fclose($output_file);
	
		}
		catch(Exception $e)
		{
			trigger_error("file_get_contents_chunked::" . $e->getMessage(), E_USER_NOTICE);
			return false;
		}
	
		return true;
	}

	private function getLine ($handle, $size, $max_lines) {
		$row = [];
		if ($max_lines > 0) 
			$line_count = 0; 
		else 
			$line_count = -1;

		
		while ($line_count < $max_lines && ($data = fgetcsv($handle, $size, ",")) !== FALSE) {
			$row[] = $data;
			if ($max_lines > 0){
				$line_count++;
			}
		}
		return $row;
	}

	private function createCsv ($data, $output_file) {
		if (!empty($data)) {
			try
			{
				foreach ($data as $line)
				{
					fputcsv($output_file, $line);
				}	
			}
			catch(Exception $e)
			{
				trigger_error("create_csv::" . $e->getMessage(),E_USER_NOTICE);
				return false;
			}		
		}	
	}
} 
?>